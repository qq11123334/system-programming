#include "apue.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <signal.h>

void put_pull_rod(int signum);
void fish_eating();
void exit_game(int signum);

int fishNum = 0;	// counting fish number
int boolean = 0;	// used as a boolean
int fishing = 0;
int fish_running = 0;
int fish_escaped = 0;
int time_limit;

void ready();
void pull();
void put();

int main(void){
	srand(time(NULL));
	struct sigaction sig_put_pull_rod;
	sig_put_pull_rod.sa_handler = put_pull_rod;
	
	struct sigaction sig_exit_game;
	sig_exit_game.sa_handler = exit_game;

	//write your code here

	sigaction(SIGINT, &sig_put_pull_rod, NULL);
	sigaction(SIGALRM, &sig_put_pull_rod, NULL);
	sigaction(SIGTSTP, &sig_exit_game, NULL);
	
	ready();
	while(1) {
		sleep(1000);
	}
	return 0;
}


void ready() {
	fishing = 0;
	fish_running = 0;
	fish_escaped = 0;
	time_limit = rand() % 5 + 1;
	printf("Fishing rod is ready!\n");
}
void put() {
	printf("\nPut the fishing rod\n");
	printf("Bait into water, waiting fish...\n");
}
void pull() {
	printf("\nPull the fishing rod\n");
}
void put_pull_rod(int signum){	
	if(signum == SIGINT) {
		if(!fishing) {
			put();
			fishing = 1;
			alarm(rand() % 5 + 1);
		} else if(fishing) {
			if(fish_running) {
				pull();
				printf("Catch a Fish!!\n");
				fish_eating();
				ready();
			} else if(fish_escaped) {
				pull();
				printf("The bait was eaten\n");
				ready();
			} else {
				pull();
				ready();
			}
		}

	}

	if(signum == SIGALRM) {
		if(fishing && !fish_running) {
			printf("A fish is biting,pull the fishing rod\n");
			fish_running = 1;
			alarm(time_limit);
		} else if(fishing && fish_running) {
			printf("This fish was escaped!!\n");
			fish_running = 0;
			fish_escaped = 1;
		}
	}
}

void fish_eating(){
	fishNum++;
}

void exit_game(int signum){
	printf("\nTotally caught fishes: %d\n", fishNum);
	exit(0);
}

