#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#define MAXLINE (4096)
int main() {
	// setvbuf(stdin, NULL, _IOFBF, MAXLINE);
	// setvbuf(stdin, NULL, _IOLBF, MAXLINE);
	// setvbuf(stdin, NULL, _IONBF, MAXLINE);
	
	char buf[MAXLINE];
	int cnt = 0;
	while((fgets(buf, MAXLINE, stdin)) != NULL) {
		cnt++;
		if(fputs(buf, stdout) == EOF) {
			printf("output error\n");
			exit(1);
		}
	}
	
	if(ferror(stdin)) {
		printf("input error\n");
		exit(1);
	}

	printf("Loop Iteration = %d\n", cnt);
	return 0;
}
