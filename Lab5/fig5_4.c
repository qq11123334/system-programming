#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#define MAXLINE (4096)
int main() {
	
	int cnt = 0;
	int c;
	while((c = getc(stdin)) != EOF) {
		cnt++;
		if(putc(c, stdout) == EOF) {
			printf("output error\n");
			exit(1);
		}
	}
	
	if(ferror(stdin)) {
		printf("input error\n");
		exit(1);
	}

	printf("Loop Iteration = %d\n", cnt);
	return 0;
}
