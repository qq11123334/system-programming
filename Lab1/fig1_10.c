#include "apue.h"
#include <sys/wait.h>
#define MAX_Parameter 3

static void	sig_int(int);		/* our signal-catching function */
int myStrtok(char *str, char *delim, char *res[]) {
        int tok_count = 0;
        char *str_copy = strdup(str);
        char *ptr = strtok(str_copy, delim);
        while(ptr != NULL) {
                res[tok_count++] = ptr;
                ptr = strtok(NULL, delim);
        }
        res[tok_count] = NULL;
        return tok_count;
}
void malloc_2D(char *str[], int n) {
        for(int i = 0;i < n;i++) {
                str[i] = malloc(MAXLINE);
        }
}
int
main(void) {
		char	buf[MAXLINE];	/* from apue.h */
		pid_t	pid;
		int		status;

		if (signal(SIGINT, sig_int) == SIG_ERR)
				err_sys("signal error");
        
        char prompt[] = "##";
		while (1) {

            /* print prompt */
            printf("%s ", prompt);           
            fgets(buf, MAXLINE, stdin);
            
            if(strlen(buf) == 1) continue; // handle input space

            if (buf[strlen(buf) - 1] == '\n')
						buf[strlen(buf) - 1] = 0; /* replace newline with null */

                if(strcmp(buf, "quit") == 0 || strcmp(buf, "q") == 0) {
                        printf("Bye\n");
                        break; // close shell
                }
                
                char *cmd[MAX_Parameter];
                malloc_2D(cmd, MAX_Parameter);

                int tok_number = myStrtok(buf, " ", cmd);
				
                if ((pid = fork()) < 0) {
						err_sys("fork error");
				} else if (pid == 0) {		
                        /* child */
                        if(strcmp(cmd[0], "cd") == 0) {
                                exit(0);
                        } else {
                                execvp(cmd[0], cmd);
						        err_ret("couldn't execute: %s", buf);
						        exit(127);
                        }
				}
                
				/* parent */
				if(strcmp(cmd[0], "cd") == 0) {
                        if(chdir(cmd[1]) == -1) {
                                printf("cd Failed\n");        
                        }
                }
                
                if ((pid = waitpid(pid, &status, 0)) < 0)
						err_sys("waitpid error");
		}
		exit(0);
}

		void
sig_int(int signo)
{
		printf("interrupt\n");
}

