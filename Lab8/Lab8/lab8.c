#include <stdio.h>
#include "apue.h"
void accumulation(int d_sum);

int main()
{
	FILE *sum;
	int total_sum = 0;
	pid_t pid[5];
	const int year = 5, week = 52, day = 7, number_of_data = 96;

	sum = fopen("sum.txt", "w");
	fprintf(sum, "%d\n", 0);
	fclose(sum);

	/**********************************************************/
	TELL_WAIT();

	for(int i = 0; i < year; i++)
	{
		pid[i] = fork();
		if(pid[i] == 0) /* child */
		{ 
			for(int j = 0; j < week; j++) 
			{
				char filename[20];
				if(j + 1 < 10) sprintf(filename, "%d-0%d.txt", i + 1, j + 1);
				else sprintf(filename, "%d-%d.txt", i + 1, j + 1);
				
				FILE *input = fopen(filename, "r");
				for(int k = 0; k < day; k++) 
				{
					int day_sum = 0;
					for(int l = 0; l < number_of_data; l++) 
					{
						int minute_input;
						fscanf(input, "%d", &minute_input);
						day_sum += minute_input;
					}
					
					WAIT_PARENT();
					accumulation(day_sum);
					TELL_PARENT(getppid());
				}
			}
			exit(0);
		}
	}

	/* parent */
	for(int i = 0; i < week; i++) 
	{
		for(int j = 0; j < day; j++) 
		{
			for(int k = 0; k < year; k++) 
			{
				TELL_CHILD(pid[k]);
				WAIT_CHILD();
			}
		}
	}
	/**********************************************************/

	sum = fopen("sum.txt", "r");
	fscanf(sum, "%d", &total_sum);
	printf("Day_Average = %d\n", total_sum / (year * week * day));
	fclose(sum);

	return 0;
}

void accumulation(int d_sum) //Accumulating the daily sum to "sum.txt".
{
	FILE *sum;
	int tmp = 0;

	sum = fopen("sum.txt", "r+");
	fscanf(sum, "%d", &tmp);

	tmp += d_sum;

	rewind(sum);
	fprintf(sum, "%d", tmp);
	fclose(sum);

	return;
}
